using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class StartLevelTrigger : MonoBehaviour
{
    [SerializeField] protected Level _level;

    private void OnTriggerEnter(Collider other)
    {
        LevelManager.Instance.StartLevel(_level);
        gameObject.SetActive(false);
    }
}
