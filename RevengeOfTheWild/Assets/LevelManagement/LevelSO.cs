using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level")]
public class LevelSO : ScriptableObject
{
    [SerializeField] public Stage[] stages;
    public float timeBetweenStages;
    public int enemiesLeftTolerance;
}

[System.Serializable]
public class Stage
{
    public EnemyEntry[] enemies;
    public float timeBetweenSpawns;
    public int groupSize;
}

[System.Serializable]
public class EnemyEntry : System.ICloneable
{
    public Enemy enemyPrefab;
    public int number;

    public object Clone()
    {
        EnemyEntry returnValue = new EnemyEntry();
        returnValue.enemyPrefab = enemyPrefab;
        returnValue.number = number;
        return returnValue;
    }
}
