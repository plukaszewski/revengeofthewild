using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoSingleton<LevelManager>
{
    public List<Level> levels = new List<Level>();

    protected int _currentLevelNumber = -1;

    public void StartNextLevel()
    {
        levels[++_currentLevelNumber].Begin();
    }

    public void StartLevel(int inNumber)
    {
        levels[inNumber].Begin();
    }

    public void StartLevel(Level inLevel)
    {
        levels.Find(x => x == inLevel).Begin();
    }
}
