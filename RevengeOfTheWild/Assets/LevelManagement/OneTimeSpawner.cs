using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneTimeSpawner : Spawner
{
    [SerializeField] protected GameObject _prefab;
    
    public void TrySpawn()
    {
        GameObject obj;
        TrySpawn(_prefab, out obj);
    }
}
