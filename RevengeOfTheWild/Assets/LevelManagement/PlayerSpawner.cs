 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    [SerializeField] protected PlayerController[] _players;
    protected PlayerController _player;

    private void Awake()
    {
        PlayerPrefs.SetInt("Player", (int)PlayerType.Skunk);
        SpawnPlayer(_players[PlayerPrefs.GetInt("Player")]);
    }

    protected void SpawnPlayer(PlayerController inPlayer)
    {
        _player = Instantiate(inPlayer, transform.position, transform.rotation);
    }

    private void Start()
    {
        Global.OnPlayerSpawn.Invoke(_player);
    }
}
