using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float minimalDistance;
    
    public bool CanSpawn()
    {
        if (Vector3.Distance(transform.position, Global.playerController.transform.position) < minimalDistance)
            return false;
        
        RaycastHit hit;
        if (Physics.Linecast(transform.position, Global.camera.transform.position, out hit))
            if (hit.transform.gameObject != Global.playerController.gameObject)
                return true;
        
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Global.camera);
        if (GeometryUtility.TestPlanesAABB(planes, GetComponent<CapsuleCollider>().bounds))
            return false;

        return true;
    }

    protected T Spawn<T>(T inPrefab) where T : Object
    {
        return Instantiate(inPrefab, transform.position, new Quaternion());
    }

    public bool TrySpawn<T>(T inPrefab, out T outObject) where T : Object
    {
        if(CanSpawn())
        {
            outObject = Spawn(inPrefab);
            return true;
        }
        outObject = null;
        return false;
    }
}
