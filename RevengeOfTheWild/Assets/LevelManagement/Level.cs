using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Level : MonoBehaviour
{
    public LevelSO info;
    public List<Enemy> enemies;

    protected int _stageNumber = -1;
    [SerializeField] protected Spawner[] _spawners;
    [SerializeField] protected OneTimeSpawner[] _oneTimeSpawners;
    protected List<EnemyEntry> _enemyList;

    public UnityEvent OnComplete;
    public UnityEvent OnStart;

    public void Begin()
    {
        OnStart.Invoke();

        foreach(var item in _oneTimeSpawners)
        {
            item.TrySpawn();
        }

        TryNextStage();
    }

    public void NextStage()
    {
        if(++_stageNumber < info.stages.Length)
        {
            _enemyList = new List<EnemyEntry>(CurrentStage().enemies.CloneValues());
            StartCoroutine(TimeBetweenSpawns());
        }
        else
        {
            OnComplete.Invoke();
        }
    }

    public void TryNextStage()
    {
        if((enemies.Count <= info.enemiesLeftTolerance && _enemyList?.Count <= 0) || _enemyList == null)
        {
            StartCoroutine(TimeBetweenStagesTimer());
        }
    }

    private void TrySpawn()
    {
        List<Spawner> list = new List<Spawner>(_spawners);
        Enemy outObject;

        for (int i = list.Count, n = 0; i > 0 && _stageNumber < info.stages.Length && n < CurrentStage().groupSize && _enemyList.Count > 0; i--)
        {
            var enemyInfo = _enemyList.RandomElement();
            var randomSpawner = list.RandomElement();
            if (randomSpawner.TrySpawn(enemyInfo.enemyPrefab, out outObject))
            {
                if(--enemyInfo.number <= 0)
                {
                    _enemyList.Remove(enemyInfo);
                }
                outObject.level = this;
                enemies.Add(outObject.GetComponent<Enemy>());
                n++;
            }
            list.Remove(randomSpawner);
        }
    }

    public Stage CurrentStage()
    {
        return info.stages[_stageNumber];
    }

    public IEnumerator TimeBetweenStagesTimer()
    {
        yield return new WaitForSeconds(info.timeBetweenStages);
        NextStage();
    }

    private IEnumerator TimeBetweenSpawns()
    {
        while(_enemyList.Count > 0)
        {
            yield return new WaitForSeconds(CurrentStage().timeBetweenSpawns);
            TrySpawn();
        }
    }

    private void Start()
    {
        LevelManager.Instance.levels.Add(this);
    }
}
