using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class HealthBar : MonoBehaviour
{
    public Health target;
    [SerializeField] private RectTransform _heartPrefab;
    [SerializeField] private RectTransform _group;
    private Slider _slider;

    private void SetTarget(PlayerController inPlayer)
    {
        target = inPlayer.GetComponent<Health>();
        for (int i = 0; i < (int)target.MaxHealth / 10; i++)
        {
            Instantiate(_heartPrefab, _group);
        }
        _slider = GetComponent<Slider>();
        target.OnHealthChange.AddListener(UpdateValue);
        StartCoroutine(SetSize());
    }

    private IEnumerator SetSize()
    {
            yield return new WaitForEndOfFrame();
            GetComponent<RectTransform>().sizeDelta = new Vector2(_group.sizeDelta.x, GetComponent<RectTransform>().sizeDelta.y);
    }

    private void UpdateValue()
    {
       // _slider.value = (((int)((target.CurrentHealth - 1) / 10) + 1) * 10 / target.MaxHealth);
        _slider.value = (target.CurrentHealth / target.MaxHealth);
    }

    private void Awake()
    {
        Global.OnPlayerSpawn.AddListener(SetTarget);
    }
}
