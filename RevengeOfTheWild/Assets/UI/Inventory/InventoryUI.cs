using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventoryUI : MonoBehaviour
{
    [SerializeField] protected Inventory _target;
    [SerializeField] protected TextMeshProUGUI _ammoText;
    [SerializeField] protected Image _weaponSprite;
    
    protected void UpdateAmmo()
    {
        RangedWeapon ranged = _target.currentWeapon as RangedWeapon;
        if(ranged)
        {
            _ammoText.text = ranged.CurrentAmmo.ToString();
        }
        else
        {
            _ammoText.text = "---";
        }
    }

    protected void UpdateSprite()
    {
        _weaponSprite.sprite = _target.currentWeapon.UISprite;
    }

    protected void Start()
    {
        
    }

    private void SetTarget(PlayerController inPlayer)
    {
        _target = inPlayer.inventory;
        foreach (var item in _target.weapons)
        {
            item.OnFire.AddListener(UpdateAmmo);
        }
        _target.OnWeaponChange.AddListener(UpdateAmmo);
        _target.OnWeaponChange.AddListener(UpdateSprite);
    }

    private void Awake()
    {
        Global.OnPlayerSpawn.AddListener(SetTarget);
    }
}
