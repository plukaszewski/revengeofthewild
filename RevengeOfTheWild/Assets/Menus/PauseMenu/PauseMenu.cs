using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public bool isPaused { get; protected set; }
    
    private void Awake()
    {
        Global.pauseMenu = this;
        UnPause();
    }

    public void Pause()
    {
        isPaused = true;
        gameObject.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 0f;
    }

    public void UnPause()
    {
        isPaused = false;
        gameObject.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1f;
    }

    public void TogglePause()
    {
        SetPause(!isPaused);
    }

    public void SetPause(bool inShouldPause)
    {
        if (inShouldPause)
        {
            Pause();
        }
        else
        {
            UnPause();
        }
    }
}
