using UnityEngine;
using UnityEngine.Events;

public class Enemy : MonoBehaviour
{
    public Level level;

    protected void OnDestroy()
    {
        level?.enemies.Remove(this);
        level?.TryNextStage();
    }
}
