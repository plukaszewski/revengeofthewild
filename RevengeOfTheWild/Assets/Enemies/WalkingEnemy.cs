using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(BoxCollider))]
public class WalkingEnemy : Enemy
{
    public float damage = 10f;
    public float attackTime = 2f;
    public BoxCollider attackHitbox;
    private NavMeshAgent _nav;
    private Animator _anim;
    private bool _isAttacking = false;

    public IEnumerator AttackTimer()
    {
        yield return new WaitForSeconds(attackTime);
        foreach (var item in Physics.BoxCastAll(transform.position, attackHitbox.size, transform.forward, new Quaternion(), attackHitbox.size.z + attackHitbox.center.z))
        {
            var tmp = item.transform.GetComponent<PlayerController>();
            if(tmp)
            {
                tmp.GetComponent<Health>().Damage(damage);
                break;
            }
        }
        _isAttacking = false;
    }

    private void Awake()
    {
        _nav = GetComponent<NavMeshAgent>();
        _anim = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isAttacking && Vector3.Distance(Global.playerController.transform.position, transform.position) >= _nav.stoppingDistance)
        {
            _nav.SetDestination(Global.playerController.transform.position);
            _anim.SetBool("IsWalking", true);
        }
        else if(!_isAttacking)
        {
            _isAttacking = true;
            _anim.SetTrigger("Attack");
            _anim.SetBool("IsWalking", false);
            StartCoroutine(AttackTimer());
        }
    }
}
