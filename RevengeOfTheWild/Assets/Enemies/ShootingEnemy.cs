using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(LineRenderer))]
public class ShootingEnemy : Enemy
{
    public float damage = 10f;
    public float shotingRange;
    public float aimingTime = 5f;
    public float shootingTime = 2f;
    public float restingTime = 1f;
    public float shootingLaserWidthModifier = 1.5f;
    private NavMeshAgent _nav;
    private Animator _anim;
    private LineRenderer _line;
    [SerializeField] private GameObject _gunPoint;
    private bool _isShooting = false;
    private bool _isAiming = false;
    private bool _isResting = false;
    private RaycastHit _shootingTargetHit;
    private Vector3 _direction;
    private float _shootingTimeCounter;

    private const string isWalkingAnimation = "isWalking";

    IEnumerator AimingTimer()
    {
        yield return new WaitForSeconds(aimingTime);
        _isAiming = false;
        _isShooting = true;
        StartCoroutine(ShootingTimer());
    }

    IEnumerator ShootingTimer()
    {
        _shootingTimeCounter = 0f;
        while (_shootingTimeCounter <= shootingTime)
        {
            _shootingTimeCounter += Time.deltaTime;
            _line.widthMultiplier = 1f + shootingLaserWidthModifier * (_shootingTimeCounter / shootingTime);
            yield return null;
        }

        _anim.SetBool("isAiming", false);
        _line.widthMultiplier = 1f;
        _isShooting = false;
        _isResting = true;
        _line.positionCount = 0;
        if (Physics.Raycast(_gunPoint.transform.position, _direction, out _shootingTargetHit))
        {
            _shootingTargetHit.transform.GetComponent<PlayerController>()?.GetComponent<Health>().Damage(damage);
        }
        StartCoroutine(RestingTimer());
    }

    IEnumerator RestingTimer()
    {
        yield return new WaitForSeconds(restingTime);
        _isResting = false;
    }

    private void Awake()
    {
        _nav = GetComponent<NavMeshAgent>();
        _anim = GetComponentInChildren<Animator>();
        _line = GetComponentInChildren<LineRenderer>();
        _nav.stoppingDistance = shotingRange;
    }
    private void Start()
    {
        //_nav.SetDestination(Global.playerController.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        _nav.SetDestination(transform.position); //reduce difference between simulated postion and real position caused by rigidbody
        if (!_isAiming && !_isShooting && !_isResting)
        {
            if (Vector3.Distance(Global.playerController.transform.position, transform.position) >= shotingRange)
            {
                _nav.SetDestination(Global.playerController.transform.position);
                _anim.SetBool(isWalkingAnimation, true);
            }
            else
            {
                _isAiming = true;
                _line.positionCount = 2;
                StartCoroutine(AimingTimer());
                _anim.SetBool(isWalkingAnimation, false);
                _anim.SetBool("isAiming", true);
            }
        }

        if (_isAiming || _isShooting)
        {
            if (_isAiming)
            {
                _direction = Global.playerController.transform.position - _gunPoint.transform.position;
            }

            Vector3 lineDestination;
            if (Physics.Raycast(_gunPoint.transform.position, _direction, out _shootingTargetHit))
            {
                lineDestination = _shootingTargetHit.point;
            }
            else
            {
                lineDestination = _direction.normalized * 100f;
            }
            transform.eulerAngles = new Vector3(0f, (transform.position.x < Global.playerController.transform.position.x ? 1f : -1f) * Vector3.SignedAngle(Vector3.forward, Global.playerController.transform.position - transform.position, Vector3.forward) + 20f, 0f); //+20f is correction to animation rotation offset
            _line.SetPositions(new Vector3[] { _gunPoint.transform.position, lineDestination });
        }
    }
}
