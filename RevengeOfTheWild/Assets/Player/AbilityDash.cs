using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AbilityDash : Ability
{
    public float useTime;
    public float speedMultiplier;

    protected float _useCounter;
    protected bool _isInUse;
    protected Movement _movement;
    protected PlayerController _playerController;
    protected Rigidbody _rigid;

    protected override IEnumerator Use()
    {
        isOnCooldown = true;
        _isInUse = true;

        _playerController.canControlMovement = false;
        float oldMovementSpeed = _movement.movementSpeed;
        _movement.movementSpeed *= speedMultiplier;
        _rigid.mass = 0f; //TODO

        Vector3 tmp = _rigid.velocity;
        Vector2 direction = new Vector2(tmp.x, tmp.z) != Vector2.zero
            ? new Vector2(tmp.x, tmp.z)
            : new Vector2(Mathf.Sin(transform.rotation.eulerAngles.y / 180f * Mathf.PI), Mathf.Cos(transform.rotation.eulerAngles.y / 180f * Mathf.PI));

        _useCounter = 0f;
        while (_useCounter <= useTime)
        {
            _movement.Move(direction);
            _useCounter += Time.deltaTime;
            yield return null;
        }

        _rigid.mass = 1;
        _movement.movementSpeed = oldMovementSpeed;
        _playerController.canControlMovement = true;
        _isInUse = false;
        _movement.ResetVelocity();
        StartCoroutine(RechargeTimer());
    }

    protected void Awake()
    {
        _movement = GetComponent<Movement>();
        _playerController = GetComponent<PlayerController>();
        _rigid = GetComponent<Rigidbody>();
    }
}
