using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public enum WeaponType
{
    Uzi,
    HandGranade,
    Knife,
    Shotgun,
    Minigun,
    GranadeLuncher,
    Club,
}

public abstract class Weapon : MonoBehaviour
{
    public float fireRate = 2f;
    public float damage = 5f;
    public Sprite UISprite;
    public WeaponType weaponType;
    protected bool _canFire = true;
    [SerializeField] protected Animator _anim;

    public UnityEvent OnFire;

    public void TryFire()
    {
        if (!CanFire())
            return;

        _canFire = false;
        BeforeFire();
        Fire();
        StartCoroutine(FireRateTimer());
    }

    protected virtual void Fire()
    {
        OnFire?.Invoke();
    }

    protected virtual void BeforeFire() { }

    protected virtual bool CanFire()
    {
        return _canFire;
    }

    public IEnumerator FireRateTimer()
    {
        yield return new WaitForSeconds(1 / fireRate);
        _canFire = true;
    }

    protected virtual void OnEnable()
    {
        if(!_canFire)
            StartCoroutine(FireRateTimer());
    }

    protected virtual void OnDisable() { }

    protected virtual void Awake() { }

    protected virtual void Start() { }
}
