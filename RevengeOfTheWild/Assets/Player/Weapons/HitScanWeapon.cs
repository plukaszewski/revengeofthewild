using UnityEngine;

public class HitScanWeapon : RangedWeapon
{
    public float range = 1000f;
    public float accuracy = 0.95f;
    
    
    protected override void Fire()
    {
        base.Fire();

        RaycastHit hitInfo;

        Vector3 direction = Global.camera.transform.forward;
        direction = new Vector3(direction.x + Random.Range(accuracy - 1f, 1f - accuracy), direction.y + Random.Range(accuracy - 1f, 1f - accuracy), direction.z + Random.Range(accuracy - 1f, 1f - accuracy));
        Debug.DrawLine(Global.camera.transform.position, direction * 100f);
        if (Physics.Raycast(Global.camera.transform.position, direction, out hitInfo, range, ~((1 << 3) + (1 << 2)))) //ignoring ignore raycast and weapon layer
        {
            Health healthComponent = hitInfo.transform.GetComponent<Health>();
            Debug.DrawLine(Global.camera.transform.position, hitInfo.point, Color.green, 5f);

            if (healthComponent != Global.playerController.GetComponent<Health>())
            {
                healthComponent?.Damage(damage);
            }
        }
        _anim.SetTrigger("Fire");
    }
}
