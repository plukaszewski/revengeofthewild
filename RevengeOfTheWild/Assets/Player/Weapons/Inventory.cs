using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Inventory : MonoBehaviour
{
    public List<Weapon> weapons = new List<Weapon>();
    public Weapon currentWeapon;
    protected int _crurrentWeaponNumber = -1;
    [SerializeField] protected Animator _anim;
    protected bool _isDuringAnimation = false;

    public UnityEvent OnWeaponChange;

    public void ChooseWeapon(int inWeaponNumber)
    {
        if(inWeaponNumber < weapons.Count && _crurrentWeaponNumber != inWeaponNumber && !_isDuringAnimation)
        {
            _isDuringAnimation = true;
            _crurrentWeaponNumber = inWeaponNumber;
            _anim.SetInteger("Weapon", (int)weapons[_crurrentWeaponNumber].weaponType);
            _anim.SetTrigger("WeaponChange");
            StartCoroutine(PerformAnimation());
        }
    }

    protected IEnumerator PerformAnimation()
    {
        yield return new WaitForEndOfFrame();
        var state = _anim.GetCurrentAnimatorStateInfo(0);
        yield return new WaitForSeconds(state.length);
        currentWeapon?.gameObject.SetActive(false);
        currentWeapon = weapons[_crurrentWeaponNumber];
        OnWeaponChange?.Invoke();
        currentWeapon.gameObject.SetActive(true);
        state = _anim.GetCurrentAnimatorStateInfo(0);
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(state.length);
        _isDuringAnimation = false;
    }

    public void ChooseNextWeapon()
    {
        if(_crurrentWeaponNumber + 1 >= weapons.Count)
        {
            ChooseWeapon(0);
        }
        else
        {
            ChooseWeapon(_crurrentWeaponNumber + 1);
        }
    }

    public void ChoosePreviousWeapon()
    {
        if (_crurrentWeaponNumber - 1 < 0)
        {
            ChooseWeapon(weapons.Count - 1);
        }
        else
        {
            ChooseWeapon(_crurrentWeaponNumber - 1);
        }
    }

    public void TryFire()
    {
        if(!_isDuringAnimation)
        {
            currentWeapon?.TryFire();
        }
    }

    public void Awake()
    {
        foreach(var item in GetComponentsInChildren<Weapon>())
        {
            if(!weapons.Contains(item))
            {
                weapons.Add(item);
            }
            item.gameObject.SetActive(false);
        }

        ChooseWeapon(0);
        _anim.ResetTrigger("WeaponChange");
        _anim.ResetTrigger("Fire");
    }
}
