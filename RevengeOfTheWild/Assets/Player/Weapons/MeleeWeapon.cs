using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MeleeWeapon : Weapon
{
    public BoxCollider range;

    protected Vector3 _boxVector;
    protected Vector3 _boxSize;

    [SerializeField] protected float _attackDelay;

    protected override void Fire()
    {
        _anim.SetTrigger("Fire");
        StartCoroutine(DoFire());
    }

    private IEnumerator DoFire()
    {
        yield return new WaitForSeconds(_attackDelay);
        base.Fire();
        Vector3 boxCenter = Global.camera.transform.rotation * _boxVector;

        Debug.DrawLine(boxCenter + Global.camera.transform.position, Global.camera.transform.position, Color.red, 2f);

        foreach (var item in ToHealth(Physics.BoxCastAll(boxCenter + Global.camera.transform.position, _boxSize, Global.camera.transform.forward, Global.camera.transform.rotation, 0f)).Distinct())
        {
            if (item != Global.playerController.GetComponent<Health>())
            {
                item?.Damage(damage);
            }
        }
    }

    private IEnumerable<Health> ToHealth(IEnumerable<RaycastHit> list)
    {
        foreach (var item in list)
        {
            Health healthComponent = item.transform.GetComponent<Health>();
            if (healthComponent)
                yield return healthComponent;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        var camera = GetComponentInParent<Camera>().transform;
        _boxVector = transform.position + transform.rotation * new Vector3(range.center.x * transform.lossyScale.x, range.center.y * transform.lossyScale.y, range.center.z * transform.lossyScale.z) - camera.position;
        _boxSize = new Vector3(range.size.x * transform.lossyScale.x, range.size.y * transform.lossyScale.y, range.size.z * transform.lossyScale.z) * .5f;
    }

}
