using UnityEngine;

public class ProjectileWeapon : RangedWeapon
{
    public Projectile projectile;
    public float force;

    protected override void Fire()
    {
        base.Fire();
        var tmp = Instantiate(projectile, transform.position + Global.camera.transform.forward, new Quaternion());
        tmp.damage = damage;
        tmp.GetComponent<Rigidbody>().velocity = Global.camera.transform.forward * force;
        _anim.SetTrigger("Fire");
    }
}
