using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Projectile : MonoBehaviour
{
    public float lifeTime = 3f;
    public float damage = 100f;

    protected IEnumerator LifeTimeTimer()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }

    protected virtual void OnDestroy()
    {

    }

    public virtual void Start()
    {
        StartCoroutine(LifeTimeTimer());
    }
}
