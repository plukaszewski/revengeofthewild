using UnityEngine;

public class RangedWeapon : Weapon
{
    [SerializeField] protected int _maxAmmo = 100;
    public int MaxAmmo { get => _maxAmmo; }
    [SerializeField] protected int _currentAmmo = 100;
    public int CurrentAmmo { get => _currentAmmo; protected set => _currentAmmo = value; }

    protected override void BeforeFire()
    {
        CurrentAmmo--;
        base.BeforeFire();
    }

    /*
    [SerializeField] protected int _magazineSize = 10;
    public int MagazineSize { get => _magazineSize;  }

    public float reloadTime = 1f;
    protected bool _isReloading = false;

    public void StartReload()
    {
        if(!_isReloading)
        {
            StartCoroutine(ReloadTimer());
        }
    }

    protected void Reload()
    {

    }
    
    public IEnumerator ReloadTimer()
    {
        yield return new WaitForSeconds(reloadTime);
        Reload();
    }
    */

    protected override bool CanFire()
    {
        return base.CanFire() /*&& !_isReloading*/ && CurrentAmmo > 0;
    }
}
