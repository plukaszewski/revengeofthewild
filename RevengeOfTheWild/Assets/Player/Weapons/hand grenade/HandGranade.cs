using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandGranade : ProjectileWeapon
{
    [SerializeField] protected float throwDelay;
    [SerializeField] protected float deactivateDelay;
    [SerializeField] protected float activateDelay;
    [SerializeField] protected GameObject projectileProp;
    
    protected override void Fire()
    {
        _anim.SetTrigger("Fire");
        StartCoroutine(DoFire());
    }

    protected virtual IEnumerator DoFire()
    {
        yield return new WaitForSeconds(deactivateDelay);
        projectileProp.SetActive(false);

        yield return new WaitForSeconds(throwDelay - deactivateDelay);
        var tmp = Instantiate(projectile, transform.position + Global.camera.transform.forward, new Quaternion());
        tmp.damage = damage;
        tmp.GetComponent<Rigidbody>().velocity = Global.camera.transform.forward * force;
        yield return new WaitForSeconds(activateDelay - throwDelay - deactivateDelay);
        projectileProp.SetActive(true);
    }
}
