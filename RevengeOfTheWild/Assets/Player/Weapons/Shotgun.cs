using UnityEngine;

public class Shotgun : HitScanWeapon
{
    public int numberOfShots = 6;

    protected override void Fire()
    {
        for(int i = 0; i < numberOfShots; i++)
        {
            base.Fire();
        }
    }
}
