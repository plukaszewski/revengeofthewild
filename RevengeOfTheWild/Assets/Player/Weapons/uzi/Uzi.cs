using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Uzi : HitScanWeapon
{
    [SerializeField] protected GameObject _secondWeapon;

    protected override void OnDisable()
    {
        base.OnDisable();
        _secondWeapon.SetActive(false);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        _secondWeapon.SetActive(true);
    }
}
