using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Granade : Projectile
{
    public float range = 100f;

    protected override void OnDestroy()
    {
        foreach (var item in ToHealth(Physics.SphereCastAll(transform.position, range, Vector3.up)).Distinct())
        {
            if (item != Global.playerController.GetComponent<Health>())
            {
                item?.Damage(damage);
            }
        }
    }

    private IEnumerable<Health> ToHealth(IEnumerable<RaycastHit> list)
    {
        foreach (var item in list)
        {
            Health healthComponent = item.transform.GetComponent<Health>();
            if (healthComponent)
                yield return healthComponent;
        }
    }
}
