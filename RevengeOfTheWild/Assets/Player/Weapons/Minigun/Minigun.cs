using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minigun : HitScanWeapon
{
    public float maxFireRate;
    public float fireRateIncreseRatio;
    public float timeToReset;

    protected float _baseFireRate;
    protected float _timeToResetCounter;
    protected bool _isOnFire = false;

    protected override void Awake()
    {
        base.Awake();
        _baseFireRate = fireRate;
    }

    protected override void BeforeFire()
    {
        base.BeforeFire();
        fireRate = Mathf.Clamp(fireRate * fireRateIncreseRatio, _baseFireRate, maxFireRate);
        _timeToResetCounter = 0f;
        if(!_isOnFire)
        {
            _isOnFire = true;
            StartCoroutine(TimeToResetTimer());
        }
    }

    protected IEnumerator TimeToResetTimer()
    {
        while (fireRate > _baseFireRate)
        {
            while (_timeToResetCounter <= timeToReset)
            {
                _timeToResetCounter += Time.deltaTime;
                _anim.SetFloat("FireSpeed", fireRate / maxFireRate);
                yield return null;
            }

            fireRate = Mathf.Clamp(fireRate / fireRateIncreseRatio, _baseFireRate, maxFireRate);
            _anim.SetFloat("FireSpeed", fireRate / maxFireRate);
            yield return new WaitForSeconds(timeToReset);
        }
        _isOnFire = false;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        if (_isOnFire)
        {
            _isOnFire = false;
            fireRate = _baseFireRate;
            _anim.SetFloat("FireSpeed", fireRate / maxFireRate);
        }
    }
}
