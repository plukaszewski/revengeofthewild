using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Movement)), RequireComponent(typeof(PlayerController))]
public class AbilityCharge : Ability
{
    public float useTime;
    public float speedMultiplier;
    public float controlRatio;
    public float collisionForce;
    public float damage;

    protected float _useCounter;
    protected bool _isInUse;
    protected Movement _movement;
    protected PlayerController _playerController;

    protected override IEnumerator Use()
    {
        isOnCooldown = true;
        _isInUse = true;

        _playerController.canControlMovement = false;
        float oldMovementSpeed = _movement.movementSpeed;
        _movement.movementSpeed *= speedMultiplier;


        _useCounter = 0f;
        while (_useCounter <= useTime)
        {
            transform.Rotate(0f, Mathf.Clamp(Input.GetAxisRaw("Mouse X"), -1f, 1f) * controlRatio, 0f);
            _movement.Move(new Vector2(Mathf.Sin(transform.rotation.eulerAngles.y / 180f * Mathf.PI), Mathf.Cos(transform.rotation.eulerAngles.y / 180f * Mathf.PI)));
            _useCounter += Time.deltaTime;
            yield return null;
        }

        _movement.movementSpeed = oldMovementSpeed;
        _playerController.canControlMovement = true;
        _isInUse = false;
        StartCoroutine(RechargeTimer());
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.rigidbody && _isInUse)
        {
            Vector3 tmp = collision.GetContact(0).point - transform.position;
            tmp.Normalize();
            collision.rigidbody.AddForce(tmp * collisionForce, ForceMode.Impulse);
            collision.transform.GetComponent<Health>()?.Damage(damage);
        }
    }

    protected void Awake()
    {
        _movement = GetComponent<Movement>();
        _playerController = GetComponent<PlayerController>();
    }
}
