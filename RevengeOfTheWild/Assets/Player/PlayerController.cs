using UnityEngine;
using UnityEngine.SceneManagement;

public enum PlayerType
{
    Mose,
    Skunk
}

[RequireComponent(typeof(Movement))]
public class PlayerController : MonoBehaviour
{
    public new Camera camera;
    public float mouseSensivity = 4f;
    public Inventory inventory;
    public bool canControlMovement = true;

    private Movement _movement;
    private Ability _ability;

    private void Die()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        SceneManager.LoadScene("MainMenu");
    }

    private void Awake()
    {
        _movement = GetComponent<Movement>();
        _ability = GetComponent<Ability>();
        Global.camera = camera;
        Global.playerController = this;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        Health health = GetComponent<Health>();
        health.OnDeath.AddListener(Die);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (canControlMovement)
        {
            _movement.Move(
                new Vector2(
                    Input.GetAxisRaw("Vertical") * Mathf.Sin(transform.rotation.eulerAngles.y / 180f * Mathf.PI) + Input.GetAxisRaw("Horizontal") * Mathf.Cos(-transform.rotation.eulerAngles.y / 180f * Mathf.PI),
                    Input.GetAxisRaw("Vertical") * Mathf.Cos(transform.rotation.eulerAngles.y / 180f * Mathf.PI) + Input.GetAxisRaw("Horizontal") * Mathf.Sin(-transform.rotation.eulerAngles.y / 180f * Mathf.PI)
                    ));
            //_movement.isSprinting = Input.GetAxisRaw("Sprint") > 0f;
            //_movement.isCrouching = Input.GetAxisRaw("Crouch") > 0f;
        }
    }

    private void Update()
    {
        if (!Global.pauseMenu.isPaused)
        {
            if (canControlMovement)
            {
                if (Input.GetAxisRaw("Jump") > 0f)
                {
                    _movement.Jump();
                }

                if (Input.GetAxisRaw("Mouse X") != 0f)
                {
                    _movement.Turn(Input.GetAxisRaw("Mouse X") * mouseSensivity);
                }
            }

            if (Input.GetAxisRaw("Mouse Y") != 0f)
            {
                //camera.transform.Rotate(-Input.GetAxisRaw("Mouse Y") * mouseSensivity, 0f, 0f);
                float YDeltaRotation = -Input.GetAxis("Mouse Y") * mouseSensivity;

                if (camera.transform.localRotation.eulerAngles.x + YDeltaRotation > 90f && camera.transform.localRotation.eulerAngles.x + YDeltaRotation < 180f)
                {
                    camera.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);
                }
                else if (camera.transform.localRotation.eulerAngles.x + YDeltaRotation < 270f && camera.transform.localRotation.eulerAngles.x + YDeltaRotation > 180f)
                {
                    camera.transform.localRotation = Quaternion.Euler(270f, 0f, 0f);
                }
                else
                {
                    camera.transform.Rotate(YDeltaRotation, 0f, 0f);
                }
            }


            if (Input.GetAxisRaw("Fire1") != 0f)
            {
                inventory.TryFire();
            }

            if (Input.GetAxisRaw("Mouse ScrollWheel") > 0f)
            {
                inventory.ChooseNextWeapon();
            }

            if (Input.GetAxisRaw("Mouse ScrollWheel") < 0f)
            {
                inventory.ChoosePreviousWeapon();
            }

            if (Input.GetAxisRaw("WeaponSlot1") != 0f)
            {
                inventory.ChooseWeapon(0);
            }

            if (Input.GetAxisRaw("WeaponSlot2") != 0f)
            {
                inventory.ChooseWeapon(1);
            }

            if (Input.GetAxisRaw("WeaponSlot3") != 0f)
            {
                inventory.ChooseWeapon(2);
            }

            if (Input.GetAxisRaw("WeaponSlot4") != 0f)
            {
                inventory.ChooseWeapon(3);
            }

            if (Input.GetAxisRaw("Ability") != 0f)
            {
                _ability?.TryUse();
            }
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Global.pauseMenu.TogglePause();
        }
    }
}
