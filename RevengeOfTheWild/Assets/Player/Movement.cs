using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Movement : MonoBehaviour
{
    public float movementSpeed = 5f;
    public float sprintMultiplayer = 1.5f;
    public float crouchMultiplayer = .5f;

    public bool isSprinting = false;
    public bool isCrouching = false;

    public float jumpForce = 10f;
    public float inAirMoveRatio = .5f;

    private Rigidbody _rigid;
    [SerializeField] private SphereCollider _groundedCollider;
    private Vector3 _lastVelocity;

    public void Move(Vector2 inDirection)
    {
        Vector2 tmp = inDirection.normalized * movementSpeed * (isSprinting ? sprintMultiplayer : 1f) * (isCrouching ? crouchMultiplayer : 1f);

        _rigid.velocity = 
            IsGrounded() 
            ? _lastVelocity = new Vector3(tmp.x, _rigid.velocity.y, tmp.y) 
            : new Vector3(tmp.x, _rigid.velocity.y, tmp.y) * inAirMoveRatio + new Vector3(_lastVelocity.x, _rigid.velocity.y, _lastVelocity.z) * (1 - inAirMoveRatio);

        /*   ^
         *   |
        if (IsGrounded())
        {
            _rigid.velocity = _lastVelocity = new Vector3(tmp.x, _rigid.velocity.y, tmp.y);
        }
        else
        {
            _rigid.velocity = new Vector3(tmp.x, _rigid.velocity.y, tmp.y) * inAirMoveRatio + new Vector3(_lastVelocity.x, _rigid.velocity.y, _lastVelocity.z) * (1 - inAirMoveRatio);
        }
        */
    }

    public void Jump()
    {
        if(CanJump())
        {
            _rigid.velocity = Vector3.up * jumpForce;
        }
    }

    public void Turn(float InAngle)
    {
        transform.Rotate(0f, InAngle, 0f);
    }

    public bool CanJump()
    {
        return IsGrounded();
    }

    public bool IsGrounded()
    {
        return Physics.SphereCastAll(_groundedCollider.center + transform.position, _groundedCollider.radius, Vector3.down, 0f, ~((1 << 3) + (1 << 2))).Length > 2; //ignoring ignore raycast and weapon layer
    }

    public void ResetVelocity()
    {
        _rigid.velocity = _rigid.velocity.normalized * movementSpeed;
    }

    private void Awake()
    {
        _rigid = GetComponent<Rigidbody>();
    }
}
