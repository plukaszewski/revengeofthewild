using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : MonoBehaviour
{
    public bool isOnCooldown;
    public float rechargeTime;

    public bool TryUse()
    {
        if (isOnCooldown)
        {
            return false;
        }

        StartCoroutine(Use());

        return true;
    }

    protected virtual IEnumerator Use()
    {
        yield return null;
    }

    protected IEnumerator RechargeTimer()
    {
        yield return new WaitForSeconds(rechargeTime);
        isOnCooldown = false;
    }
}
