using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility
{
    public static T RandomElement<T>(this IList<T> inList)
    {
        return inList[Random.Range(0, inList.Count)];
    }

    public static IList<T> CloneValues<T>(this IList<T> inList) where T : class, System.ICloneable
    {
        List<T> returnValue = new List<T>();

        foreach (var item in inList)
        {
            returnValue.Add(item.Clone() as T);
        }

        return returnValue;
    }
}
