using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class Global
{
    public static Camera camera;
    public static PlayerController playerController;
    public static Canvas canvas;
    public static PauseMenu pauseMenu;
    public static UnityEvent<PlayerController> OnPlayerSpawn = new UnityEvent<PlayerController>();
}
