using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public float regenerationRate = 5f;
    public float regenerationDelay = 5f;
    public float regenerationDeltaTime = 1f;
    public bool destroyOnDeath = true;

    public UnityEvent OnDeath;
    public UnityEvent OnHealthChange;

    [SerializeField] private float _currentHealth = 100f;
    public float CurrentHealth
    {
        get => _currentHealth;
        private set
        {
            if (value <= 0f)
            {
                _currentHealth = 0f;

                OnDeath.Invoke();

                if(destroyOnDeath)
                {
                    Destroy(gameObject);
                }
            }
            else if (value < _maxHealth)
            {
                _currentHealth = value;
            }
            else
            {
                _currentHealth = _maxHealth;
            }

            OnHealthChange.Invoke();

            foreach (var item in GetComponentsInChildren<Renderer>())
            {
                item.material.color = Color.Lerp(Color.red, Color.white, CurrentHealth / MaxHealth);
            }
        }
    }

    [SerializeField] private float _maxHealth = 100f;
    public float MaxHealth 
    { 
        get => _maxHealth;
        set
        {
            float tmp = value - _maxHealth;
            _maxHealth = value;
            CurrentHealth += tmp > 0f ? tmp : 0f;
        }
    }

    public void Damage(float InDamage)
    {
        CurrentHealth -= InDamage;
        StopAllCoroutines();
        StartCoroutine(RegenerationDelayTimer());
    }

    public void Heal(float InHealth)
    {
        CurrentHealth += InHealth;
    }

    public IEnumerator RegenerationDelayTimer()
    {
        yield return new WaitForSeconds(regenerationDelay);
        StartCoroutine(RegenrationDeltaTimeTimer());
    }

    public IEnumerator RegenrationDeltaTimeTimer()
    {
        while (true)
        {
            Heal(regenerationRate * regenerationDeltaTime);
            yield return new WaitForSeconds(regenerationDeltaTime);
        }
    }
}